" this is to show what mode you're in
let g:currentmode={
       \ 'n'      : ' ',
       \ 'v'      : '',
       \ 'V'      : '',
       \ "\<C-V>" : '',
       \ 'i'      : '',
       \ 'R'      : 'x',
       \ 'Rv'     : 'x',
       \ 'c'      : '',
       \}

" init statusline
set statusline=

" shows the currently active mode
set statusline+=%#Visual#
set statusline+=\ [%{toupper(g:currentmode[mode()])}]

" show modified indicator
set statusline+=%{&modified?'[!]':''}

" right justify from here on
set statusline+=%=

" show current and total lines number
set statusline+=words\ \:\ %{wordcount().words}

" separator
set statusline+=\ \|\ 

" show full file path
set statusline+=%f
