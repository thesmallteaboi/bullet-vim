"""""""""""""""""""""""""""""""""""""""
" GENERAL CONFIGS FILE                "
"""""""""""""""""""""""""""""""""""""""
" Note: You shouldn't change this, you should make any change you want to
" the custom.vim file

" spell
set complete+=kspell

" set tab to be 4 spaces
set tabstop=4
set shiftwidth=4

set textwidth=0 " goyo workaround

" highlight the line in which the cursor is
hi CursorLine cterm=NONE ctermbg=242
set cursorline

let g:limelight_conceal_ctermfg = 240
let g:limelight_conceal_guifg = '#83a598'

" which-key config
set timeoutlen=0
hi WhichKeyFloat ctermbg=NONE guifg=NONE

" change splits styling
hi VertSplit guifg=reverse guifg=reverse cterm=reverse guifg=Gray
set fillchars+=vert:\ 

set scrolloff=8 " Slightly better looking scrolloff

" supertab
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContextDefaultCompletionType="<C-n>"
let g:vimwiki_table_mappings = 0

" markdown
let g:vim_markdown_strikethrough = 1
au FileType vimwiki setlocal syntax=markdown

" vimwiki
let g:vimwiki_list = [{'path': '$HOME/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}] " set vimwiki to use markdown
let g:vimwiki_autowriteall = 1 " make VimWiki autosave

" splash screen
source $VIMPATH/extra/splashscreen.vim

" file explorer
source $VIMPATH/extra/fileexplorer.vim

" other things
set noshowmode " don't show mode in the command line, we have that in the statusline!

""""""""""""""""""""""
" Filetypes          "
""""""""""""""""""""""
au BufRead,BufNewFile *.fountain set filetype=fountain " fountain
