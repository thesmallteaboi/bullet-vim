" comma is the superior leader key
let mapleader = ' '
let maplocalleader=','

"""""""""""""""""""""""""""""""""""""""""""
" Convenience things                      "
"""""""""""""""""""""""""""""""""""""""""""
" Let's make some shortcuts easier to hit!
nnoremap <C-s> :w<cr>
nnoremap <C-q> :q<cr>
nnoremap <C-S> :w!<cr>
nnoremap <C-Q> :q!<cr>

"""""""""""""""""""""""""""""""""""""""""""
" Writing Things                          "
"""""""""""""""""""""""""""""""""""""""""""
" move lines using shift+direction
nnoremap <S-j> :m .+1<CR>==
vnoremap <S-j> :m '>+1<CR>gv=gv
nnoremap <S-Up> :m .-2<CR>==
vnoremap <S-Up> :m '<-2<CR>gv=gv
nnoremap <S-k> :m .-2<CR>==
vnoremap <S-k> :m '<-2<CR>gv=gv
nnoremap <S-Down> :m .+1<CR>==
vnoremap <S-Down> :m '>+1<CR>gv=gv

""""""""""""""""""""""""""""""""""""""""""""
" Hacks                                    "
""""""""""""""""""""""""""""""""""""""""""""
" Move by visual line instead of 'physical' line	
noremap <expr> j v:count ? 'j' : 'gj'
noremap <expr> k v:count ? 'k' : 'gk'
noremap <expr> <Down> v:count ? 'j' : 'gj'
noremap <expr> <Up> v:count ? 'k' : 'gk'
