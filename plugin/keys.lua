local wk = require("which-key")

wk.register({
  s = {
	name = 'Spellcheck',
	a = {'zg'                                           ,'Add word to dictionary', noremap=false,},
	s = {'z='                                           ,'Show suggestions', noremap=false},
	S = {'1z='                                          ,'Auto correct word under cursor', noremap=false},
	t = {':set invspell<cr>'                            ,'Toggle spellcheck'},
	n = {']s'                                           ,'Next misspell'},
	p = {'[s'                                           ,'Previous misspell'},
  },

  z = {
	name = "Zen Mode",
	z = {':TZAtaraxis<cr>'                              ,'Toggle Goyo'},
	l = {':Limelight!!<cr>'                             ,'Toggle Limelight'},
  },

  w = {
	name = 'VimWiki',
	h = {':e $HOME/.config/nvim/docs/index.wiki<cr>'    ,'Open BVim help'},
    w = {                                                'Open wiki'},
    t = {                                                'Open wiki in a new tab'},
    s = {                                                'Select and open wiki index file'},
    d = {                                                'Delete wiki file you are in'},
    r = {                                                'Rename wiki file you are in'},
	i = {                                                'Open Default diary index'},
  },

  d = {
	name = 'Debug',
	r = {':so %<cr>'                                    ,'Source current file'},
  },
  i = {
	name = 'Insert',
	D = {":put! =strftime('%d/%m/%y %H:%M:%S')<cr>"     ,'Date & Time'},
	d = {":put! =strftime('%m/%d/%y')<cr>"              ,'Date'},
	t = {":put! =strftime('%H:%M:%S')<cr>"              ,'Time'},
  },
  o = {
	name = 'Open',
	h = {':e $HOME/.config/nvim/docs/index.wiki<cr>'    ,'Help'},
	f = {':Fern %:h<cr>'                                ,'File Browser'},
	d = {':Dashboard<cr>'                               ,'Dashboard'},
  },
}, { prefix = "<leader>" })

