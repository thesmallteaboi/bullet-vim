"""""""""""""""""""""""""""""""""""""""""
" INIT FILE                             "
" This file basically only reads from   "
" others                                "
"""""""""""""""""""""""""""""""""""""""""

let $VIMPATH=stdpath('config')

source $VIMPATH/custom.vim
