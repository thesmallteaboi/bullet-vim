# Keybinds

	Key Combo                        : Function                                                

## BVim
	- <Leader> o h                   : Open BVim help
	- <Leader> o d                   : Open dashboard

## Editor
	- Ctrl + q                       : Close Editor
	- Shift + Up/Down (N-V)          : Move current/selected line up/down
	- <Leader> i D                   : Insert date and time
	- <Leader> i d                   : Insert date
	- <Leader> i t                   : Insert time

## File Manipulation
	- Ctrl + s                       : Save file
		
## Distraction-free writing
	- <Leader> Space                 : Toggle Goyo
	- <Leader> <Leader> Space        : Toggle Lightlime

## VimWiki
	- <Leader> w w                   : Open default wiki index file.
	- <Leader> w t                   : Open default wiki index file in a new tab.
	- <Leader> w s                   : Select and open wiki index file.
	- <Leader> w d                   : Delete wiki file you are in.
	- <Leader> w r                   : Rename wiki file you are in.
	- <Enter>                        : Follow/Create wiki link.
	- <Shift-Enter>                  : Split and follow/create wiki link.
	- <Ctrl-Enter>                   : Vertical split and follow/create wiki link.
	- <Backspace>                    : Go back to parent(previous) wiki link.
	- <Tab>                          : Find next wiki link.
	- <Shift-Tab>                    : Find previous wiki link.
	- =                              : Add header level

## Todo.txt
	- <LeaderTwo> s                  : Sort the file
	- <LeaderTwo> s +                : Sort the file on +Projects
	- <LeaderTwo> s @                : Sort the file on @Contexts
	- <LeaderTwo> s d                : Sort the file on dates
	- <LeaderTwo> s d d              : Sort the file on due dates
	- <LeaderTwo> j                  : Decrease the priority of the current line
	- <LeaderTwo> k                  : Increase the priority of the current line
	- <LeaderTwo> a                  : Add the priority (A) to the current line
	- <LeaderTwo> b                  : Add the priority (B) to the current line
	- <LeaderTwo> c                  : Add the priority (C) to the current line
	- <LeaderTwo> d                  : Set current task's creation date to the current date
	- <LeaderTwo> x                  : Mark current task as done
	- <LeaderTwo> X                  : Mark all tasks as done
	- <LeaderTwo> D                  : Move completed tasks to done.txt

## Spell Check
	- <Leader> s s                   : Show suggestions
	- <Leader> s a                   : Add word to dictionary
	- <Leader> s p                   : Go to previous misspell
	- <Leader> s n                   : Go to next misspell
	- <Leader> s t                   : Toggle spellcheck
