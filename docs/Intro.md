# Introduction
## What is *BVim*?
	Vim is very powerful as a text editor, which makes it perfect for... Well, writing. This isn't a surprise to anybody, but nobody seems to realize just how useful Vim could be to write notes, TODO lists, or even entire books!
	This, plus the fact that I love vim, made me start this project.
	I'm not a coder, but I really enjoy editing text in Vim, so I want to do it as much as possible! This arises a problem, though... I'm not a coder, so I cannot promise the code here will be optimal or even work, but I'll try my best.
