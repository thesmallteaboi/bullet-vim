#!/bin/sh
# shellcheck disable=SC2015,SC2086

echo 'checking for nvim config'
nvimDir="${XDG_CONFIG_HOME:-$HOME/.config}/nvim"
[ -d "$nvimDir" ] && {
	echo 'nvim config detected, **deleting it**'
	rm -rf "$nvimDir"
:;} || {
	echo 'no nvim config detected'
}

packerDir="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/start/packer.nvim"
echo 'checking for packer'
[ -d "$packerDir" ] && {
	echo 'packer already installed, doing nothing'
:;} || {
	echo "packer isn't installed, installing it"
	git clone https://github.com/wbthomason/packer.nvim "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/packer/start/packer.nvim"
}

echo 'making personal config file'
cp ./custom.vim.skeleton ./custom.vim
echo 'symlinking config'
ln -s "${PWD}" "${XDG_CONFIG_HOME:-$HOME/.config}/nvim"
