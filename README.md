# Bullet Vim
###### A Neovim config for writers
A minimalistic-ish config, specially made to write notes, stories, novels or whatever other
human-readable text you want!

## But... Why?
Well, I love vim. I can't code. I want to use vim. Let's use vim to write other things.

I also just love configuring things :D

## Features
- Fast (ish)
- Minimal (ish)
- Pretty (or at least I hope so)
- Feature full (it has everything you'll ever need, and more!)
- Easy to install (literally one command!)

## Quick start
### Requisites
- nvim 0.5
- git
- [Node.js](https://nodejs.org/en/)
- [Python 3+](https://www.python.org/downloads/)
	- also install `neovim` with pip
- [Nerd Fonts](https://www.nerdfonts.com/)
		
### Install
To install (on Linux), run
```sh
git clone https://gitlab.com/thesmallteaboi/bullet-vim
cd bullet-vim
chmod +x install.sh
./install.sh
```
(Note: This will **delete** any previous neovim config you may have, please backup)

### Config
All your configurations should go in `~./config/nvim/custom.vim` (or $XDG_CONFIG_HOME/nvim/custom.vim if that's set)

This file won't be replaced when you update Bullet Vim.

***

> "Consult your lawyer if you plan to use this"

\- Prince

Made with love (and vim)
