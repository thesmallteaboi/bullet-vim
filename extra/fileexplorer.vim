let g:fern#renderer = "nerdfont"
let g:fern#default_hidden = 1

function! s:init_fern() abort
nmap <buffer><expr>
      \ <Plug>(fern-my-open-expand-collapse)
      \ fern#smart#leaf(
      \   "\<Plug>(fern-action-open:select)",
      \   "\<Plug>(fern-action-expand)",
      \   "\<Plug>(fern-action-collapse)",
      \ )
nmap <buffer> <Tab> <Plug>(fern-my-open-expand-collapse)
endfunction
