let g:dashboard_custom_header = [
			\"88888b  88    88 88      88      8888888 88888888      88    88 88888888  888  888 ",
			\"88   88 88    88 88      88      88         88         88    88    88    88 8888 88",
			\"888888  88    88 88      88      88888      88         88    88    88    88  88  88",
			\"888888  88    88 88      88      88888      88          88  88     88    88  88  88",
			\"88   88 88    88 88      88      88         88           8888      88    88  88  88",
			\"88888P   888888  8888888 8888888 8888888    88            88    88888888 88  88  88",
            \"                                                                                   ",
            \"                            A config for the vim writer                            ",
			\]

let g:dashboard_custom_footer = [
            \'                                                         ⢠⣿⣶⣄⣀               ',
            \'                                                       ⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣦⣄⣀⡀⣠⣾⡇    ',
            \'                                                     ⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇    ',
            \'___      __          __     ___          ___   /    ⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠿⢿⣿⣿⡇    ',
            \' |  \ / |__) | |\ | / _`     |  |  |\/| |__   / ⣶⣿⣦⣜⣿⣿⣿⡟⠻⣿⣿⣿⣿⣿⣿⣿⡿⢿⡏⣴⣺⣦⣙⣿⣷⣄   ',
            \' |   |  |    | | \| \__>     |  |  |  | |___ .  ⣯⡇⣻⣿⣿⣿⣿⣷⣾⣿⣬⣥⣭⣽⣿⣿⣧⣼⡇⣯⣇⣹⣿⣿⣿⣿⣧  ',
            \'                                                 ⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠸⣿⣿⣿⣿⣿⣿⣿⣷',
            \]

let g:dashboard_custom_section={
  \ 'open_wiki': {
      \ 'description': ['  Open Wiki    '],
	  \ 'command': 'e ~/vimwiki/index.md' },
  \ 'open_diary': {
      \ 'description': ['  Open Diary   '],
	  \ 'command': 'e ~/vimwiki/diary/diary.md' },
  \ 'new_file': {
      \ 'description': ['﬒  New File     '],
      \ 'command': 'enew' },
  \ 'open_file': {
      \ 'description': ['  Open File    '],
	  \ 'command': 'Fern .' },
  \ 'open_help': {
      \ 'description': ['  Open Help    '],
	  \ 'command': 'e $VIMPATH/docs/index.md' },
  \ }
